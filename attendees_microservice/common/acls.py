import json
import requests
from events.keys import OPEN_WEATHER_API_KEY, PEXELS_API_KEY

def get_photo(city, state):

    response = requests.get(f'https://api.pexels.com/v1/search?query={city} {state} building', headers={'Authorization':PEXELS_API_KEY})
    parsed_text = json.loads(response.text)
    print(city, state)
    return parsed_text["photos"][0]['src']["large"]

def get_weather(city, state):
    response = requests.get(f'https://api.openweathermap.org/data/2.5/weather?q={city},{state},US&appid={OPEN_WEATHER_API_KEY}')
    parsed_text = json.loads(response.text)
    temp = round(((parsed_text["main"]["temp"]-273.15)*1.8+32), 2)
    descr = parsed_text["weather"][0]["main"]
    return {"temp": temp, "description": descr}
